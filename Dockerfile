FROM google/cloud-sdk:405.0.0-alpine

RUN gcloud components install kubectl \
    && curl -O https://get.helm.sh/helm-v3.2.0-linux-amd64.tar.gz \
    && tar xvf helm-v3.2.0-linux-amd64.tar.gz \
    && mkdir -p $HOME/bin && mv ./linux-amd64/helm $HOME/bin/helm && export PATH=$PATH:$HOME/bin \
    && rm -rf linux-amd64 && rm -rf helm-v3.2.0-linux-amd64.tar.gz

CMD ["/bin/sh"]
